// MQTT publisher

var mqtt = require('mqtt')
var client = mqtt.connect('mqtt://localhost:8081')
var topic = 'topic-bp'
var message = 'Hello IOT world !'

client.on('connect', () => {
    setInterval(() => {
        client.publish(topic, message)
        console.log('Message sent !')
    }, 5000)
})
